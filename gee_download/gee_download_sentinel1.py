import os
import folium
import ee
from datetime import datetime
from tqdm import tqdm
import pandas as pd
import argparse

OUTPUT_DIR = "gee_download_output"
if not os.path.isdir(OUTPUT_DIR):
    os.mkdir(OUTPUT_DIR)
YEARS = [2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022]
# YEARS = [2018]
# B4 is red, B3 is green, B2 is blue, B8 is NIR
# https://developers.google.com/earth-engine/datasets/catalog/COPERNICUS_S2_SR#bands
# CLD_MASK_NAME = 'cloudmask'
# BANDS = ['B4', 'B3', 'B2', 'B8', CLD_MASK_NAME]

DRIVE_FOLDER = "Sentinel1_"

CITY_CSV = r"city_aoi/building_list.csv"



def getSentinalS1Image(aoi, *years):
    city_imgs= []
    orbit = "ASCENDING"
    for year in years[0]:
        start_date = str(year) + '-01-01'
        end_date = str(year) + '-12-31'
        s1_col = (ee.ImageCollection('COPERNICUS/S1_GRD')
                  .filterBounds(aoi)
                  .filterDate(start_date, end_date)
                  # .filter(ee.Filter.eq('orbitProperties_pass', orbit))
                  .filter(ee.Filter.listContains('transmitterReceiverPolarisation', 'VV'))
                  .filter(ee.Filter.listContains('transmitterReceiverPolarisation', 'VH'))
                  .filter(ee.Filter.eq('instrumentMode', 'IW'))
                  .select(["VV", "VH"])
                  )

        print("Find {} images of Sentinel1 SAR for year {} ".format(s1_col.size().getInfo(), year))
        city_img = ee.Image.cat(s1_col.mean())

        city_img = city_img.set({"year": year})
        city_imgs.append(city_img)

    city_imgcol = ee.ImageCollection.fromImages(city_imgs)
    return city_imgcol


def export2GDrive(img_collection, city_name, aoi):
    # empty tasks list
    tasklist = []
    # get region

    # Make a list of images
    img_list = img_collection.toList(img_collection.size())

    n = 0
    while True:
        try:
            img = ee.Image(img_list.get(n))
            city_year = img.getInfo()["properties"]["year"]
            description = city_name + str(city_year)
            task = ee.batch.Export.image.toDrive(image=img,
                                                 description=description,
                                                 folder=DRIVE_FOLDER + city_name,
                                                 fileNamePrefix=description,
                                                 region=aoi,
                                                 scale=10
                                                 )
            task.start()
            print("exporting {} to folder '{}' in GDrive".format(description, DRIVE_FOLDER))

            tasklist.append(task)
            n += 1
        except Exception as e:
            error = str(e).split(':')
            if error[0] == 'List.get':
                break
            else:
                raise e

    return tasklist


if __name__ == "__main__":
    # Note: The authentication with gcloud CLI is needed. After the gcloud is installed, the restart of PC may be need.
    # Trigger the authentication flow.
    ee.Authenticate()
    # Initialize the library.
    ee.Initialize()

    # with open(r"key_path_ethAccount/service_account.txt") as f:
    #     service_account = f.read().replace('\n', '')
    # credentials = ee.ServiceAccountCredentials(service_account, r"key_path_ethAccount/.private-key.json")
    # ee.Initialize(credentials)

    start_time = datetime.now()

    cities = pd.read_csv(CITY_CSV)

    for i in tqdm(range(len(cities))):
        # if i < 30:
        #     continue
        city_name = cities.iloc[i]["city"]
        min_lon = cities.iloc[i]["min_lon"]
        max_lon = cities.iloc[i]["max_lon"]
        min_lat = cities.iloc[i]["min_lat"]
        max_lat = cities.iloc[i]["max_lat"]

        coords = [
            [min_lon, min_lat],
            [max_lon, min_lat],
            [max_lon, max_lat],
            [min_lon, max_lat],
            [min_lon, min_lat]
        ]
        aoi = ee.Geometry.Polygon(coords)

        print("City--{}".format(city_name))

        city_imgcol = getSentinalS1Image(aoi, YEARS)
        if city_imgcol is not None:
            export2GDrive(city_imgcol, city_name, aoi)

    end_time = datetime.now()
    print("Script Run Time - Start: ", start_time.strftime("%Y-%m-%d %H:%M:%S"))
    print("Script Run Time -   End: ", end_time.strftime("%Y-%m-%d %H:%M:%S"))
