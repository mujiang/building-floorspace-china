# Building Floorspace China

# Project tree
 * [1. Download from Google Earth Engine](./gee_download)
   * [1.1. City Area of Interest](./gee_download/city_aoi) 
  

 * [2. U-net model](./u-net-model)
   * [2.1. Building ground truth conversion](./u-net-model/building-ground-truth-conversion.ipynb)
   * [2.2. Data preparation](./u-net-model/data-prepare.ipynb) 
   * [2.3. Training model](./u-net-model) 
     * [Sentinel 1](./u-net-model/u-net-sentinel1-training.ipynb)
     * [Sentinel 2](./u-net-model/u-net-sentinel2-training.ipynb)
     * [Sentinel 1 and 2](./u-net-model/u-net-combo-training.ipynb)
   * [2.3.1. Model weights](./u-net-model/model_weights)
   * [2.4. Model prediction](./u-net-model/u-net-prediction.ipynb) 
   * [2.5. Validation images](./u-net-model/val-image) 
     * [City example: Sanya](./u-net-model/sentinel2_val_image)
   * [2.6. Two-stage model](./u-net-model/2-stage-model) 


## 1. Downloading Sentinel 1 and 2 from Google Earth engine 

### 1.1. City Area of Interest 
The areas of interest for each city are acquired from the crowd-sourced dataset used in earlier work, such as Cao and Huang (2021). 


## 2. U-net model 

### 2.1. Building ground truth conversion
This code is primarily for converting vector ground truth data into the raster format. Major operations include coordinate system projection, tile clipping into the same range and cell size as the Sentinel satellite imagery and reclassifying height range (0-6, 9-27, 30+). This might need the ArcGIS software installed on your computer.

### 2.2. Data preparation
This code is for processing the Sentinel satellite imagery and building ground truth raster data. Each large image is split into 64x64 sub-images. Here we provide functions for processing (1) Sentinel 1, (2 Sentinel 2, (3) Sentinel 1 + Sentinel 2 when specifying the "*mode*" parameter. The results can be saved either together or separately under different city folders when specifying the "*save_separate*" parameter. If you wish to process only selected cites the parameter *“save_selected”* shall be set *True* and the list *“city_list_selected”* shall be specified. 

### 2.3. U-net training 
Training for Sentinel 1, 2 and 1+2 (combo) U-net model. Model weights (2.3.1.) are saved in the "*model_weights*" folder. Need to specify training data path, epoch, batch size, image size and image dimension.

### 2.4. U-net prediction
Model prediction using selected model weights. The evaluation metrics include accuracy, recall, precision, weighted dice coefficient and dice coefficients for each class, regarding both height and footprint. By setting (*save_example = True*) you may then save 100 image pairs for manual inspection. 

### 2.5. Validation images 
100 examples we obtained after running the u-net model on the validation dataset. 
In `./u-net-model/val-imageval_sheet.xlsx`, we keep the manual annotation result we discuss in Section 3.2 and Appendix B.

### 2.6. Two-stage model
we have tested two-stage models with Sentinel 1 + 2 images. In the two-stage models, we first predict only the footprints of the buildings (the first stage) and then feed the predicted footprints into a height prediction model (the second stage). We have validated three architectures in the second stage of the two-stage models when predicting heights: (1) stacking the building footprint prediction results and the Sentinel images together as height prediction model inputs; (2) clipping Sentinel images with building footprint prediction results before putting them into the height prediction model; (3) training in parallel the building footprint prediction and height prediction in the encoder part, and use skip connection to concatenate them for the bottleneck and decoder parts of the U-Net model. 


